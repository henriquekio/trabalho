<?php
/**
 * Created by PhpStorm.
 * User: Henrique
 * Date: 27/01/2017
 * Time: 00:05
 */

namespace actions;


abstract class Action
{
    protected $pagina;
    protected $view;

    public function __construct()
    {
        $this->view = new \stdClass;
    }

    protected function content()
    {
        include_once '../app/view/' . $this->pagina . ".phtml";
    }

    protected function redirecionaPagina($pagina)
    {
        $this->pagina = $pagina;
        include "../app/view/layout.phtml";
    }
}