<?php
/**
 * Created by PhpStorm.
 * User: Henrique
 * Date: 29/01/2017
 * Time: 23:31
 */

namespace app\model;


class Disciplina
{
    private $id;
    private $nome;
    private $id_curso;
    protected $db;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function selecionaTodasDisciplinas(){
        $query = "SELECT d.id, d.nome, d.id_curso, c.nome AS curso FROM disciplina d JOIN curso c on c.id = d.id_curso ORDER BY c.nome, d.nome";
        return $this->db->query($query);
    }

    public function selecionaDisciplina(){
        $query = "SELECT d.id, d.nome, d.id_curso, c.nome AS curso FROM disciplina d JOIN curso c on c.id = d.id_curso ORDER BY c.nome, d.nome";
        return $this->db->query($query);
    }

    public function insereDisciplina(){
        $query = "INSERT INTO disciplina (nome, id_curso) VALUES ('$this->nome', '$this->id_curso')";
        $this->db->query($query);
    }

    public function excluiDisciplina($id){
        $query = "DELETE FROM disciplina WHERE id = $id";
        $dados = $this->db->prepare($query);
        $dados->execute();
    }

    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    public function getIdCurso()
    {
        return $this->id_curso;
    }

    public function setIdCurso($id_curso)
    {
        $this->id_curso = $id_curso;
    }


}