<?php
/**
 * Created by PhpStorm.
 * User: Henrique
 * Date: 27/01/2017
 * Time: 01:21
 */

namespace app\model;


class Curso
{
    private $id;
    private $nome;
    protected $db;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function selecionaTodosCursos(){
        $query = "SELECT * FROM curso";
        return $this->db->query($query);
    }

    public function selecionaCurso($id){
        $query = "SELECT * FROM curso WHERE id = $id";
        return $this->db->query($query);
    }

    public function insereCurso(){
        $query = "INSERT INTO curso (nome) VALUES ('$this->nome')";
        $this->db->query($query);
    }

    public function excluiCurso($id){
        $query = "DELETE FROM curso WHERE id = $id";
        $dados = $this->db->prepare($query);
        $dados->execute();
    }

    public function alteraCurso($id){
        $dados = $this->selecionaCurso($id);
        $this->setNome($dados->nome);
        $query = "UPDATE curso SET nome = $this->nome  WHERE id = '.$id.'";
        $this->db->query($query);
    }


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;
    }


}