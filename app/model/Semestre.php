<?php
/**
 * Created by PhpStorm.
 * User: Henrique
 * Date: 30/01/2017
 * Time: 01:44
 */

namespace app\model;


class Semestre
{
    private $id_curso;
    private $id_disciplina;
    private $semestre;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }


    public function insereSemestre(){
        $query = "INSERT INTO semestre (id_curso, id_disciplina, semestre) VALUES ($this->id_curso, $this->id_disciplina, $this->semestre)";
        $this->db->query($query);
    }

    public function relatorioCurso(){
        $query = "SELECT c.nome as curso, s.semestre, d.nome as disciplina FROM semestre s join curso c ON c.id = s.id_curso join disciplina d on s.id_disciplina = d.id GROUP BY curso";
        $dados = $this->db->prepare($query);
        $dados->execute();
        $resultado = $dados->fetchAll(\PDO::FETCH_ASSOC);
        return $resultado;
    }
      public function relatorioSemestre(){
        $query = "SELECT c.nome as curso, s.semestre, d.nome as disciplina FROM semestre s join curso c ON c.id = s.id_curso join disciplina d on s.id_disciplina = d.id ORDER BY c.nome, semestre";
        $dados = $this->db->prepare($query);
        $dados->execute();
        $resultado = $dados->fetchAll(\PDO::FETCH_ASSOC);
        return $resultado;
    }
      public function relatorioDisciplina(){
        $query = "SELECT c.nome as curso, s.semestre, d.nome as disciplina FROM semestre s join curso c ON c.id = s.id_curso join disciplina d on s.id_disciplina = d.id GROUP BY curso";
        $dados = $this->db->prepare($query);
        $dados->execute();
        $resultado = $dados->fetchAll(\PDO::FETCH_ASSOC);
        return $resultado;
    }



    public function getIdCurso()
    {
        return $this->id_curso;
    }

    public function setIdCurso($id_curso)
    {
        $this->id_curso = $id_curso;
    }

    public function getIdDisciplina()
    {
        return $this->id_disciplina;
    }

    public function setIdDisciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    public function getSemestre()
    {
        return $this->semestre;
    }

    public function setSemestre($semestre)
    {
        $this->semestre = $semestre;
    }


}