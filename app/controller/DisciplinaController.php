<?php
/**
 * Created by PhpStorm.
 * User: Henrique
 * Date: 29/01/2017
 * Time: 23:16
 */

namespace app\controller;


use actions\Action;
use app\Conexao;
use app\model\Curso;
use app\model\Disciplina;


class DisciplinaController extends Action
{
    public function iniciarCadastroDisciplina()
    {
        if ($_POST['nome']) {
            $disciplina = new Disciplina(Conexao::getDb());
            $disciplina->setNome($_POST['nome']);
            $disciplina->setIdCurso($_POST['id_curso']);
            $disciplina->insereDisciplina();
        }
        $redireciona = new IndexController();
        $redireciona->disciplina();
    }

    public function cadastroDisciplina()
    {
        $curso = new Curso(Conexao::getDb());
        $this->view->cursos = $curso->selecionaTodosCursos();
        $this->redirecionaPagina("cadastroDisciplina");
    }

    public function excluiDisciplina(){
        $id = $_GET['id'];
        $disciplina = new Disciplina(Conexao::getDb());
        $disciplina->excluiDisciplina($id);
        $redireciona = new IndexController();
        $redireciona->disciplina();
    }
}