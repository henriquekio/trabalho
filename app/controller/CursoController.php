<?php
/**
 * Created by PhpStorm.
 * User: Henrique
 * Date: 29/01/2017
 * Time: 23:15
 */

namespace app\controller;


use actions\Action;
use app\Conexao;
use app\model\Curso;

class CursoController extends Action
{


    public function cadastroCurso()
    {
        $redireciona = new IndexController();
        $redireciona->redirecionaPagina("cadastroCurso");
    }

    public function iniciarCadastroCurso()
    {
        if ($_POST['nome']) {
            $curso = new Curso(Conexao::getDb());
            $curso->setNome($_POST['nome']);
            $curso->insereCurso();
        }
        $redireciona = new IndexController();
        $redireciona->curso();
    }

    public function excluiCurso(){
        $curso = $_GET['id'];
        $dados = new Curso(Conexao::getDb());
        $dados->excluiCurso($curso);
        $redireciona = new IndexController();
        $redireciona->curso();
    }
}