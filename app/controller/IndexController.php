<?php
/**
 * Created by PhpStorm.
 * User: Henrique
 * Date: 26/01/2017
 * Time: 22:54
 */

namespace app\controller;


use actions\Action;
use app\Conexao;
use app\model\Curso;
use app\model\Disciplina;

class IndexController extends Action
{

    public function index()
    {
        $this->redirecionaPagina("inicio");
    }

    public function curso()
    {
        $curso = new Curso(Conexao::getDb());
        $this->view->cursos = $curso->selecionaTodosCursos();
        $this->redirecionaPagina("curso");
    }


    public function disciplina()
    {
        $disciplina = new Disciplina(Conexao::getDb());
        $this->view->disciplinas = $disciplina->selecionaTodasDisciplinas();
        $this->redirecionaPagina("disciplina");
    }

    public function vinculo(){
        $disciplina = new Disciplina(Conexao::getDb());
        $curso = new Curso(Conexao::getDb());
        $this->view->disciplinas = $disciplina->selecionaTodasDisciplinas();
        $this->view->cursos = $curso->selecionaTodosCursos();
        $this->redirecionaPagina("vinculo");
    }

}