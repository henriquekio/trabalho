<?php
/**
 * Created by PhpStorm.
 * User: Henrique
 * Date: 30/01/2017
 * Time: 02:01
 */

namespace app\controller;


use actions\Action;
use app\Conexao;
use app\model\Semestre;

class SemestreController extends Action
{

    public function insereSemestre()
    {
        $semestre = new Semestre(Conexao::getDb());
        $semestre->setIdCurso($_POST['id_curso']);
        $semestre->setIdDisciplina($_POST['id_disciplina']);
        $semestre->setSemestre($_POST['semestre']);
        $semestre->insereSemestre();
        $redireciona = new IndexController();
        $redireciona->index();
    }

    public function relatorio(){
        $relatorio = new Semestre(Conexao::getDb());
        $this->view->dadosCurso = $relatorio->relatorioCurso();
        $this->view->dadosSemestre = $relatorio->relatorioSemestre();
        $this->view->dadosDisciplina = $relatorio->relatorioDisciplina();
        $this->redirecionaPagina("relatorio");
    }
}