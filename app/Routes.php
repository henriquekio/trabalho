<?php
/**
 * Created by PhpStorm.
 * User: Henrique
 * Date: 26/01/2017
 * Time: 21:25
 */

namespace app;


class Routes
{
    private $routes;


    public function __construct()
    {
        $this->iniciaRotas();
        $this->carregaRotas($this->getUrl());
    }

    public function iniciaRotas(){
        $rota["home"] = array('route'=>'/', 'controller' => 'indexController', 'action' => 'index');
        $rota["inicio"] = array('route'=>'/inicio', 'controller' => 'indexController', 'action' => 'index');
        $rota["curso"] = array('route'=>'/curso', 'controller' => 'indexController', 'action' => 'curso');
        $rota["cadastro-de-curso"] = array('route'=>'/cadastro-de-curso', 'controller' => 'cursoController', 'action' => 'cadastroCurso');
        $rota["iniciarCadastroCurso"] = array('route'=>'/iniciarCadastroCurso', 'controller' => 'cursoController', 'action' => 'iniciarCadastroCurso');
        $rota["excluiCurso"] = array('route'=>'/excluiCurso', 'controller' => 'cursoController', 'action' => 'excluiCurso');
        $rota["disciplina"] = array('route'=>'/disciplina', 'controller' => 'indexController', 'action' => 'disciplina');
        $rota["cadastro-de-disciplina"] = array('route'=>'/cadastro-de-disciplina', 'controller' => 'disciplinaController', 'action' => 'cadastroDisciplina');
        $rota["iniciarCadastroDisciplina"] = array('route'=>'/iniciarCadastroDisciplina', 'controller' => 'disciplinaController', 'action' => 'iniciarCadastroDisciplina');
        $rota["excluiDisciplina"] = array('route'=>'/excluiDisciplina', 'controller' => 'disciplinaController', 'action' => 'excluiDisciplina');
        $rota["vinculo"] = array('route'=>'/vinculo', 'controller' => 'indexController', 'action' => 'vinculo');
        $rota["vinculaSemestre"] = array('route'=>'/vincula-semestre', 'controller' => 'semestreController', 'action' => 'insereSemestre');
        $rota["relatorio"] = array('route'=>'/relatorio', 'controller' => 'semestreController', 'action' => 'relatorio');
        $this->setRoutes($rota);
    }

    public function carregaRotas($url){
        array_walk($this->routes, function($rota) use ($url){
            if($url == $rota['route']){
                $class = "app\\controller\\".ucfirst($rota['controller']);
                $controller = new $class;
                $action = $rota['action'];
                $controller->$action();
            }
        });
    }

    public function setRoutes(array $rotas){
        $this->routes = $rotas;
    }

    public function getUrl(){
        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }

}